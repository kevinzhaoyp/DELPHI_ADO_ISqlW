object SqlQryFrm: TSqlQryFrm
  Left = 379
  Top = 240
  Width = 579
  Height = 407
  ActiveControl = MoSql
  Caption = #26597#35810
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 12
  object SBar: TStatusBar
    Left = 0
    Top = 354
    Width = 571
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = #23601#32490
  end
  object PnlMain: TPanel
    Left = 0
    Top = 0
    Width = 571
    Height = 354
    Align = alClient
    BevelOuter = bvNone
    Caption = 'PnlMain'
    TabOrder = 1
    OnResize = PnlMainResize
    object Splitter1: TSplitter
      Left = 0
      Top = 130
      Width = 571
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      OnMoved = Splitter1Moved
    end
    object MoSql: TMemo
      Left = 0
      Top = 0
      Width = 571
      Height = 130
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Courier New'
      Font.Style = []
      HideSelection = False
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      WantTabs = True
    end
    object Pgc1: TPageControl
      Left = 0
      Top = 133
      Width = 571
      Height = 221
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 1
      TabPosition = tpBottom
      object TabSheet1: TTabSheet
        Caption = #32593#26684
        object SbxResult: TScrollBox
          Left = 0
          Top = 0
          Width = 563
          Height = 196
          VertScrollBar.Tracking = True
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 0
          object PnlResult: TPanel
            Left = 0
            Top = 0
            Width = 563
            Height = 97
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #28040#24687
        ImageIndex = 1
        object MoMsg: TMemo
          Left = 0
          Top = 0
          Width = 563
          Height = 196
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 
      'SQL Files (*.sql)|*.sql|Text Files (*.txt)|*.txt|All Files (*.*)' +
      '|*.*'
    Left = 116
    Top = 161
  end
  object SaveDialog1: TSaveDialog
    Filter = 
      'SQL Files (*.sql)|*.sql|Text Files (*.txt)|*.txt|All Files (*.*)' +
      '|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 188
    Top = 161
  end
  object GridMenu: TPopupMenu
    Left = 180
    Top = 265
    object MnCopyGridText: TMenuItem
      Caption = #22797#21046
      OnClick = MnCopyGridTextClick
    end
  end
end
