unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ImgList, ActnList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Buttons, StdActns, Pub, ADOInt, ObjectsBrowser;

type
  TMainForm = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    ActionList: TActionList;
    ImgList: TImageList;
    ActConnect: TAction;
    ActDisConnect: TAction;
    ActAllDisConnect: TAction;
    ActQuit: TAction;
    ActAbout: TAction;
    N8: TMenuItem;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton23: TToolButton;
    ToolButton24: TToolButton;
    PnlObjectBrowser: TPanel;
    BtnHideObjectBrowser: TSpeedButton;
    Splitter1: TSplitter;
    Label1: TLabel;
    Panel2: TPanel;
    StatusBar1: TStatusBar;
    ActExecute: TAction;
    CbxDatabases: TComboBox;
    ToolButton13: TToolButton;
    ActCancelExec: TAction;
    Q1: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    EditSelectAll1: TEditSelectAll;
    EditUndo1: TEditUndo;
    E1: TMenuItem;
    U1: TMenuItem;
    N11: TMenuItem;
    T1: TMenuItem;
    C1: TMenuItem;
    P1: TMenuItem;
    S1: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    ActNew: TAction;
    SearchFind1: TSearchFind;
    SearchFindNext1: TSearchFindNext;
    SearchReplace1: TSearchReplace;
    N14: TMenuItem;
    Find1: TMenuItem;
    FindNext1: TMenuItem;
    Replace1: TMenuItem;
    W1: TMenuItem;
    WindowClose1: TWindowClose;
    WindowCascade1: TWindowCascade;
    WindowTileHorizontal1: TWindowTileHorizontal;
    WindowTileVertical1: TWindowTileVertical;
    WindowMinimizeAll1: TWindowMinimizeAll;
    WindowArrange1: TWindowArrange;
    Cascade1: TMenuItem;
    ileHorizontally1: TMenuItem;
    ileVertically1: TMenuItem;
    N15: TMenuItem;
    ActOpen: TAction;
    ActSave: TAction;
    ActSaveAs: TAction;
    O1: TMenuItem;
    S2: TMenuItem;
    A1: TMenuItem;
    N16: TMenuItem;
    ActPrint: TAction;
    P2: TMenuItem;
    ActParse: TAction;
    IL_TV: TImageList;
    ActObjBrowser: TAction;
    N17: TMenuItem;
    N18: TMenuItem;
    ActResultBox: TAction;
    N19: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ActConnectExecute(Sender: TObject);
    procedure ActExecuteExecute(Sender: TObject);
    procedure ActExecuteUpdate(Sender: TObject);
    procedure ActCancelExecExecute(Sender: TObject);
    procedure ActCancelExecUpdate(Sender: TObject);
    procedure ActNewExecute(Sender: TObject);
    procedure ActNewUpdate(Sender: TObject);
    procedure ActQuitExecute(Sender: TObject);
    procedure CbxDatabasesDropDown(Sender: TObject);
    procedure CbxDatabasesChange(Sender: TObject);
    procedure ActOpenExecute(Sender: TObject);
    procedure ActOpenUpdate(Sender: TObject);
    procedure ActSaveExecute(Sender: TObject);
    procedure ActSaveUpdate(Sender: TObject);
    procedure ActSaveAsExecute(Sender: TObject);
    procedure ActSaveAsUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActParseExecute(Sender: TObject);
    procedure ActParseUpdate(Sender: TObject);
    procedure ActObjBrowserExecute(Sender: TObject);
    procedure ActObjBrowserUpdate(Sender: TObject);
    procedure BtnHideObjectBrowserClick(Sender: TObject);
    procedure ActAboutExecute(Sender: TObject);
    procedure ActResultBoxExecute(Sender: TObject);
    procedure ActResultBoxUpdate(Sender: TObject);
  private
    FOldActivateFrm: TForm;
    FObjBrowser: TObjectsBrowserPanel;

    function CanExecute: Boolean;

    procedure WMQryFrmActivate(var Msg: TMessage); message WM_QRYFRMACTIVATE;
    procedure WMQryFrmClose(var Msg: TMessage); message WM_QRYFRMCLOSE;
    procedure WMDatabaseChanged(var Msg: TMessage); message WM_DATABASECHANGED;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Login, SqlQry, About;

{$R *.dfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
var
  C: _Connection;
begin
  Self.BoundsRect := Screen.WorkAreaRect;
  FObjBrowser := TObjectsBrowserPanel.Create(Self);
  FObjBrowser.Parent := PnlObjectBrowser;
  FObjBrowser.Align := alClient;

  C := CoConnection.Create;
  Pub.ADOVer := C.Version;
end;

procedure TMainForm.ActConnectExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
  frmLog: TLoginFrm;
begin
  frmLog := TLoginFrm.Create(nil);
  try
    if frmLog.ShowModal = mrOK then
    begin
      frm := TSqlQryFrm.Create(Application);
      try
//        frm.UserName := frmLog.UserName;
//        frm.Password := frmLog.Password;
//        frm.AuthType := frmLog.AuthType;
//        frm.Server := frmLog.Server;
        frm.Initialize(frmLog.Connection);

        frm.BringToFront;
      except
        frm.Free;
        raise;
      end;

      FObjBrowser.AddServer(frmLog.Server, frmLog.UserName, frmLog.Password,
                frm.ServerName, frm.SUserName, frmLog.AuthType);
//      FObjBrowser.AddServer(frm.Server, frm.UserName, frm.Password, frm.ServerName,
//                frm.SUserName, frm.AuthType);
    end;
  finally
    frmLog.Free;
  end;
end;

procedure TMainForm.ActExecuteExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm <> nil then
    frm.Execute;
end;

procedure TMainForm.ActExecuteUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := CanExecute;
  CbxDatabases.Enabled := TAction(Sender).Enabled;
end;

procedure TMainForm.ActParseExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm <> nil then
    frm.ParseSQL;
end;

procedure TMainForm.ActParseUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := CanExecute;
end;

procedure TMainForm.ActCancelExecExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm <> nil then
    frm.CancelExecute;
end;

procedure TMainForm.ActCancelExecUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := (Self.ActiveMDIChild <> nil) and not CanExecute;
end;

function TMainForm.CanExecute: Boolean;
var
  frm: TForm;
begin
  frm := Self.ActiveMDIChild;
  if frm = nil then
    Result := False
  else
    Result := not (frm as TSqlQryFrm).Executing
end;

procedure TMainForm.ActNewExecute(Sender: TObject);
var
  frm, curFrm: TSqlQryFrm;
  Conn: _Connection;
begin
  curFrm := Self.ActiveMDIChild as TSqlQryFrm;
  if CurFrm = nil then Exit;

  Conn := CoConnection.Create;
  Conn.ConnectionString := curFrm.ConnectionString;
  Conn.Open('', '', '', -1);

  frm := TSqlQryFrm.Create(Application);
  try
//    frm.UserName := CurFrm.UserName;
//    frm.Password := CurFrm.Password;
//    frm.AuthType := CurFrm.AuthType;
//    frm.Server := CurFrm.Server;
    frm.Initialize(Conn);

    frm.Database := CurFrm.Database;
    frm.BringToFront;
  except
    frm.Free;
    Application.HandleException(Self);
  end;
end;

procedure TMainForm.ActNewUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := (Self.ActiveMDIChild <> nil);
end;

procedure TMainForm.ActQuitExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.ActOpenExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;

  frm.LoadFile;
end;

procedure TMainForm.ActOpenUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := (Self.ActiveMDIChild <> nil);
end;

procedure TMainForm.ActSaveExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;

  frm.SaveFile;
end;

procedure TMainForm.ActSaveUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := (Self.ActiveMDIChild <> nil);
end;

procedure TMainForm.ActSaveAsExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;

  frm.SaveAs;
end;

procedure TMainForm.ActSaveAsUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := (Self.ActiveMDIChild <> nil);
end;

procedure TMainForm.CbxDatabasesDropDown(Sender: TObject);
var
  frm: TSqlQryFrm;
  list: TStringList;
  I: Integer;
begin
  if Self.ActiveMDIChild = nil then Exit;

  frm := Self.ActiveMDIChild as TSqlQryFrm;
  list := nil;
  Screen.Cursor := crHourGlass;
  try
    list := frm.GetDatabaseList;
    CbxDatabases.Items.Assign(list);
    I := CbxDatabases.Items.IndexOf(frm.Database);
    if I >= 0 then
      CbxDatabases.ItemIndex := I;
  finally
    list.Free;
    Screen.Cursor := crDefault;
  end;
end;

procedure TMainForm.CbxDatabasesChange(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  if Self.ActiveMDIChild = nil then Exit;
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  frm.Database := CbxDatabases.Items[CbxDatabases.ItemIndex];
end;

procedure TMainForm.WMQryFrmActivate(var Msg: TMessage);
var
  frm: TSqlQryFrm;
  I: Integer;
begin
  // 子窗口成为活动窗口.

  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;
  if frm = Self.FOldActivateFrm then Exit;

  // 更新当前数据库.
  I := CbxDatabases.Items.IndexOf(frm.Database);
  if I = -1 then
    I := CbxDatabases.Items.Add(frm.Database);
  CbxDatabases.ItemIndex := I;
  FOldActivateFrm := frm;
end;

procedure TMainForm.WMQryFrmClose(var Msg: TMessage);
begin
  // 子窗口关闭.

  if ActiveMDIChild = nil then
  begin
    CbxDataBases.Clear;
  end;
end;

procedure TMainForm.WMDatabaseChanged(var Msg: TMessage);
var
  frm: TSqlQryFrm;
  I: Integer;
begin
  // 子窗口的当前数据库已经改变.

  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;
  I := CbxDatabases.Items.IndexOf(frm.Database);
  if I = -1 then
    I := CbxDatabases.Items.Add(frm.Database);
  CbxDatabases.ItemIndex := I;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//
end;

procedure TMainForm.ActObjBrowserExecute(Sender: TObject);
begin
  if PnlObjectBrowser.Visible then
  begin
    PnlObjectBrowser.Visible := False;
    Splitter1.Visible := False;
  end
  else
  begin
    Splitter1.Visible := True;
    PnlObjectBrowser.Visible := True;
  end;
end;

procedure TMainForm.ActObjBrowserUpdate(Sender: TObject);
begin
  TAction(Sender).Checked := PnlObjectBrowser.Visible;
end;

procedure TMainForm.BtnHideObjectBrowserClick(Sender: TObject);
begin
  ActObjBrowser.Execute;
end;

procedure TMainForm.ActAboutExecute(Sender: TObject);
var
  frm: TAboutBox;
begin
  frm := TAboutBox.Create(nil);
  try
    frm.ShowModal;
  finally
    frm.Free;
  end;
end;

procedure TMainForm.ActResultBoxExecute(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  if frm = nil then Exit;

  frm.ToggleResultBox;
end;

procedure TMainForm.ActResultBoxUpdate(Sender: TObject);
var
  frm: TSqlQryFrm;
begin
  frm := Self.ActiveMDIChild as TSqlQryFrm;
  TAction(Sender).Checked := (frm <> nil) and (frm.ResultBoxVisible);
end;

end.
