unit Pub;

interface
uses Messages, ADOInt, ComObj;

type
  TAuthType = (atWindows, atSqlServer);

var
  NewFileCount: Integer = 1;
  ADOVer: string;

const
  WM_QRYFRMACTIVATE = WM_USER + 10;
  WM_QRYFRMCLOSE = WM_USER + 11;
  WM_DATABASECHANGED = WM_USER + 12;
  
function CheckAutoResult(ResultCode: HResult): HResult;

implementation
uses ActiveX;

procedure SafeCallError(ErrorCode: Integer);
var
  ErrorInfo: IErrorInfo;
  Source, Description, HelpFile: WideString;
  HelpContext: Longint;
begin
  HelpContext := 0;
  if GetErrorInfo(0, ErrorInfo) = S_OK then
  begin
    ErrorInfo.GetSource(Source);
    ErrorInfo.GetDescription(Description);
    ErrorInfo.GetHelpFile(HelpFile);
    ErrorInfo.GetHelpContext(HelpContext);
  end;
  raise EOleException.Create(Description, ErrorCode, Source,
    HelpFile, HelpContext);
end;

function CheckAutoResult(ResultCode: HResult): HResult;
begin
  if ResultCode < 0 then
  begin
    SafeCallError(ResultCode);  // loses error address
  end;
  Result := ResultCode;
end;

end.
